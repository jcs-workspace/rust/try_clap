/**
 * $File: args.rs $
 * $Date: 2024-04-30 02:47:36 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright © 2024 by Shen, Jen-Chieh $
 */

use clap::{ Args, Parser, Subcommand };

#[derive(Debug, Parser)]
#[clap(author, version, about)]
pub struct RustflixArgs {
    /// The first argument
    pub first_arg: String,
    /// The second argument
    pub second_arg: String,
    /// The third argument
    pub third_arg: String,
}
